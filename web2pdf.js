var casper = require("casper").create();
casper.start();

casper.page.paperSize = {
	width: '11in',
	height: '8.5in',
	orientation: 'landscape',
	border: '0.4in'
};

casper.thenOpen('index.html', function() {
	this.capture('test.pdf');
	this.echo('created pdf.');
});

casper.run();
